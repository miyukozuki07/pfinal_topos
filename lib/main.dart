import 'dart:async';

import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(Topos());

class Topos extends StatefulWidget {
  @override
  State<Topos> createState() => _ToposState();
}

class _ToposState extends State<Topos> {

  GestureDetector prueba1 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: 160, height: 160, child: Image.asset("img/hoyo.png"),),);
  GestureDetector prueba2 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: 160, height: 160, child: Image.asset("img/hoyo.png"),),);
  GestureDetector prueba3 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: 160, height: 160, child: Image.asset("img/hoyo.png"),),);
  GestureDetector prueba4 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: 160, height: 160, child: Image.asset("img/hoyo.png"),),);
  GestureDetector prueba5 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: 160, height: 160, child: Image.asset("img/hoyo.png"),),);
  GestureDetector prueba6 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: 160, height: 160, child: Image.asset("img/hoyo.png"),),);
  Timer? timer;
  static const maxSeconds = 60;
  int seconds = maxSeconds;
  int puntaje = 0;
  double ancho = 160;
  double alto = 160;


  void startTimer(){
    timer = Timer.periodic(Duration(seconds: 1), (_){
      if(seconds > 0){
        setState(()=>seconds--);
        salidaTopos();
      }
      //puntaje = 0;
    });
  }

  void salidaTopos(){
    int random = 0;
    int contador = 60;
    while(contador > 0){
      if(contador % 2 == 0){
        random = Random().nextInt(6);
      }
      if(random == 0){
        setState(() {
          prueba1 = GestureDetector(
              child: AnimatedContainer(
                alignment: Alignment.bottomCenter,
                duration: Duration(seconds: 1),
                width: ancho,
                height: alto,
                child: Image.asset("img/topo.png"),),
              onTap: (){setState(() {
                if(seconds > 0){
                  puntaje += 50;
                  print(puntaje);
                };
          });});
        });
      }
      else{
        setState(() {
          prueba1 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: ancho, height: alto, child: Image.asset("img/hoyo.png"),),);
        });
      }
      if(random == 1){
        setState(() {
          prueba2 = GestureDetector(
              child: AnimatedContainer(
                alignment: Alignment.bottomCenter,
                duration: Duration(seconds: 1),
                width: ancho,
                height: alto,
                child: Image.asset("img/topo.png"),),
              onTap: (){setState(() {
                if(seconds > 0){
                  puntaje += 50;
                  print(puntaje);
                };
              });});
        });
      }
      else{
        setState(() {
          prueba2 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: ancho, height: alto, child: Image.asset("img/hoyo.png"),),);
        });
      }
      if(random == 2){
        setState(() {
          prueba3 = GestureDetector(
              child: AnimatedContainer(
                alignment: Alignment.bottomCenter,
                duration: Duration(seconds: 1),
                width: ancho,
                height: alto,
                child: Image.asset("img/topo.png"),),
              onTap: (){setState(() {
                if(seconds > 0){
                  puntaje += 50;
                  print(puntaje);
                };
              });});
        });
      }
      else{
        setState(() {
          prueba3 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: ancho, height: alto, child: Image.asset("img/hoyo.png"),),);
        });
      }
      if(random == 3){
        setState(() {
          prueba4 = GestureDetector(
              child: AnimatedContainer(
                alignment: Alignment.bottomCenter,
                duration: Duration(seconds: 1),
                width: ancho,
                height: alto,
                child: Image.asset("img/topo.png"),),
              onTap: (){setState(() {
                if(seconds > 0){
                  puntaje += 50;
                  print(puntaje);
                };
              });});
        });
      }
      else{
        setState(() {
          prueba4 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: ancho, height: alto, child: Image.asset("img/hoyo.png"),),);
        });
      }
      if(random == 4){
        setState(() {
          prueba5 = GestureDetector(
              child: AnimatedContainer(
                alignment: Alignment.bottomCenter,
                duration: Duration(seconds: 1),
                width: ancho,
                height: alto,
                child: Image.asset("img/topo.png"),),
              onTap: (){setState(() {
                if(seconds > 0){
                  puntaje += 50;
                  print(puntaje);
                };
              });});
        });
      }
      else{
        setState(() {
          prueba5 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: ancho, height: alto, child: Image.asset("img/hoyo.png"),),);
        });
      }
      if(random == 5){
        setState(() {
          prueba6 = GestureDetector(
              child: AnimatedContainer(
                alignment: Alignment.bottomCenter,
                duration: Duration(seconds: 1),
                width: ancho,
                height: alto,
                child: Image.asset("img/topo.png"),),
              onTap: (){setState(() {
                if(seconds > 0){
                  puntaje += 50;
                  print(puntaje);
                };
              });});
        });
      }
      else{
        setState(() {
          prueba6 = GestureDetector(child: AnimatedContainer(alignment: Alignment.bottomCenter, duration: Duration(seconds: 1), width: ancho, height: alto, child: Image.asset("img/hoyo.png"),),);
        });
      }
      contador--;
    }
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
   home: Scaffold(
    appBar: AppBar(title: Text("Pegale al Topo!"),),
     body:
     Container(
       width: 60000,
       height: 60000,
       decoration: BoxDecoration(
         image: DecorationImage(
           image: AssetImage("img/Pradera.jpg"),
           fit: BoxFit.cover,
         ),
       ),
       child:
       Column(
         mainAxisAlignment: MainAxisAlignment.end,
         children: [
           Column(
             mainAxisAlignment: MainAxisAlignment.start,
             children: [
               buildButtons(),
               buildReload(),
               buildTime(),
               const SizedBox(height: 20),
               buildScore(),
             ],
           ),
           Wrap(
             direction: Axis.horizontal,
             alignment: WrapAlignment.spaceAround,
             spacing: 5,
             runSpacing: 5,
             children: [
               prueba1,

               prueba2,

               prueba3,

               prueba4,

               prueba5,

               prueba6,

             ],
           )

         ],
       )
     ),

   ),
  );
  Widget buildButtons(){
    return ButtonWidget(
      text: 'Iniciar juego!',
      onClicked: (){
        startTimer();
    }
    );
  }

  Widget buildReload(){
    return ButtonWidget(
        text: 'Reiniciar juego',
        onClicked: (){
          seconds = 60;
          puntaje = 0;
        }
    );
  }

  Widget buildScore(){
    return Text(
      'Puntaje: $puntaje',
      style: TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.red,
        fontSize: 35,
      ),
    );
  }

  Widget buildTime(){
    return Text(
      'Tiempo: $seconds',
      style: TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.red,
        fontSize: 35,
      ),
    );
  }
}



class ButtonWidget extends StatelessWidget{
  final String text;
  final VoidCallback onClicked;
  const ButtonWidget({
    Key? key,
    required this.text,
    required this.onClicked,
}) : super(key: key);

  @override
  Widget build(BuildContext context) => ElevatedButton(
    child: Text(
      text,
      style: TextStyle(fontSize: 30),
    ),
    onPressed: onClicked,
  );
}


